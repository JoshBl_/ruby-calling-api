#requiring the class for http client
require 'net/http'
#require the URI class
require 'uri'
#requiring the json module for deserialising JSON objects
require 'json'

#get base currency from user
puts "Welcome to Josh Bank! Please enter your base currency! (In the form of three characters - such as GBP, USD, JPY)"
base = gets.chomp.upcase

#output of get request stored in a variable to deserialise JSON
json_string = Net::HTTP.get(URI.parse('https://api.exchangeratesapi.io/latest?base=' + base))

#testing output - just to make sure
puts "Direct output \n#{json_string}"

puts "First JSON Parse attempt\n"
JSON.parse(json_string).each do |currency, rate|
    puts "#{currency} - #{rate}"
end

#JSON is parsed into a hash format and the hash is iterated (on rates specifically)
#for every rate it finds in the hash - it will print the key (currency) and the value (rate)
puts "Stackover Flow"
JSON.parse(json_string)['rates'].each do |currency, rate|
    puts "#{currency} - #{rate}"
end

#parse the JSON object to a hash and iterate through it, displaying each key, value pair
#however, this doesn't work as base and date are strings, not hashes!
puts "JSON Parse output with nested each statements \n"
JSON.parse(json_string).each do |key, value|
    #however - this contains a nested hash! So a nested each do is needed.
    value.each do |currency, rate|
        puts "#{currency} - #{rate}"
    end
end
